const messageService = require('../services/message');

exports.sendMessage = async (req, res) => {
  const { message } = req.body;

  try {
    await messageService.createSession();
  } catch (error) {
    console.log(error);
  }

  try {
    const response = await messageService.sendMessage({
      message_type: 'text',
      text: message, // start conversation with empty message
    });

    const responseProcessed = await messageService.processResponse(response);

    res.json({ messageResponse: responseProcessed });
  } catch (error) {
    console.log(error);
  }
};

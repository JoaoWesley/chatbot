const assistantService = require('../services/assistantService').getAssistantService();
const constants = require('../config/constants');

let sessionId;
// Create session.
exports.createSession = async () => {
  try {
    const sessionResponse = await assistantService
      .createSession({
        assistant_id: constants.ASSISTANTID,
      });

    sessionId = sessionResponse.session_id;
  } catch (error) {
    console.log(error); // something went wrong
  }
};


// Send message to assistant.
exports.sendMessage = async (messageInput) => {
  try {
    const response = await assistantService
      .message({
        assistant_id: constants.ASSISTANTID,
        session_id: sessionId,
        input: messageInput,
      });
    return response;
  } catch (error) {
    console.log(error); // something went wrong
  }
  return null;
};

// Process the response.
exports.processResponse = (response) => {
  // text response.
  if (response.output.generic) {
    if (response.output.generic.length > 0) {
      if (response.output.generic[0].response_type === 'text') {
        // console.log(response.output.generic[0].text);
        return response.output.generic[0].text;
      }
    }
  }
  return null;
};

exports.closeSession = () => {
  // We're done, so we close the session.
  assistantService.deleteSession({
    assistant_id: constants.ASSISTANTID,
    session_id: sessionId,
  })
    .catch((err) => {
      console.log(err); // something went wrong
    });
};

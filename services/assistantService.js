const AssistantV2 = require('ibm-watson/assistant/v2');
const constants = require('../config/constants');

// Example 1: sets up service wrapper, sends initial message, and
// receives response.

// Set up Assistant service wrapper.
const service = new AssistantV2({
  iam_apikey: constants.IAM_APIKEY, // replace with API key
  version: constants.ASSISTANT_VERSION,
});

exports.getAssistantService = () => service;

const express = require('express');
const messageController = require('../controllers/messageController');

const apiRoutes = express.Router();

apiRoutes.post('/message', messageController.sendMessage);

module.exports = apiRoutes;

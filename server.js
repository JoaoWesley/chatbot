const express = require('express');

const app = express();
const bodyParser = require('body-parser');

app.all('*', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const apiRoutes = require('./routes/apiRoutes');

app.use('/api', apiRoutes);

app.listen(process.env.PORT || 3000);
